import Ajax from './ajax.js';

export class BalanceBox extends HTMLElement {
    static get observedAttributes(){
        return []
    }

    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});
        const template = `
            <style>
                * {
                    box-sizing: border-box;
                }
                .amount {
                    font-weight: bold;
                }
            </style>
            <div class="payment-box">
                Balance: <span id="balance" class="amount">0</span>
            </div>
        `;
        this._shadowRoot.innerHTML = template;
        this._balanceElement = this._shadowRoot.querySelector("#balance");

        this._shadowRoot.ownerDocument.addEventListener('payment-updated', e => this.onPaymentUpdated(e));
    }

    onPaymentUpdated(){
        Ajax.get('https://localhost:8081/balance')
          .then((response) => {
              const data = JSON.parse(response);
              return this._balanceElement.innerText = data.total;
          })
          .catch((error) => {
              console.log(error);
          });
    }

    connectedCallback() {
        this.onPaymentUpdated();
    }
}
customElements.define('balance-box', BalanceBox);
class Ajax {
  static get(url) {
    return new Promise((res, rej) => {
      const xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
      xhr.open('GET', url);
      xhr.onreadystatechange = function () {
        if (xhr.readyState > 3) {
          if (xhr.status < 400) {
            res(xhr.responseText);
          } else {
            rej(xhr);
          }
        }
      };
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.send();
    });
  }

  static post(url, data) {
    return new Promise((res, rej) => {
      const params = typeof data == 'string' ? data : JSON.stringify(data);

      const xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
      xhr.open('POST', url, true);
      xhr.onreadystatechange = function () {
        if (xhr.status < 400) {
          res(xhr.responseText);
        } else {
          rej(xhr);
        }
      };
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(params);
    });
  }
}

export default Ajax;



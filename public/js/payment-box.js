import Ajax from './ajax.js';

class PaymentBox extends HTMLElement {
    static get observedAttributes(){
        return ["sender"]
    }

    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});
        const template = `
            <style>
                * {
                    box-sizing: border-box;
                }
                .payment-box {
                    width: 100%;
                }
                .block {
                    display: block;
                    width: 100%;
                    padding: 2px 10px;
                    margin-bottom: 10px;
                }
                .non-editable {
                    background-color: #B5B5B5;
                    font-weight: bold;
                }
            </style>
            <div class="payment-box">
                From:
                <span id="sender" class="block non-editable">x</span>
                To:
                <input id="recipient" type=text class="block">
                Amount:
                <input id="value" type=number class="block">
                <button>send</button>
            </div>
        `;
        this._shadowRoot.innerHTML = template;
        this._recipientElement = this._shadowRoot.querySelector("#recipient");
        this._senderElement = this._shadowRoot.querySelector("#sender");
        this._valueElement = this._shadowRoot.querySelector("#value");
        this._shadowRoot.querySelector("button").addEventListener("click", () => {
            this.pay(this.sender, this._recipientElement.value, this._valueElement.value)
              .then(() => {
                  this.reset();
                  this._recipientElement.focus();
              });
        });
    }

    reset(){
        this._recipientElement.value = '';
        this._valueElement.value = '';
    }

    pay(sender, recipient, amount){
        return Ajax.post('https://localhost:8081/operation', {
            type: 'transfer',
            amount: amount,
            from: sender,
            to: recipient
        })
          .then((response) => {
              this.dispatchEvent(new CustomEvent("payment-updated", {bubbles: true}));
          })
          .catch((error) => {
              console.log(error);
          });
    }

    get sender(){
        return this.getAttribute("sender")
    }

    set sender(value){
        return this.setAttribute("sender", value)
    }

    connectedCallback() {   
        this._senderElement.innerHTML = this.sender;
    }
}
customElements.define('payment-box', PaymentBox);
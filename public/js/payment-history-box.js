import Ajax from './ajax.js';

class PaymentHistoryBox extends HTMLElement {
  static get observedAttributes() {
    return []
  }

  constructor() {
    super();
    this._shadowRoot = this.attachShadow({mode: 'open'});
    const template = `
            <style>
                * {
                    box-sizing: border-box;
                }
                .payment-history-box {
                    width: 100%;
                }
                .payment-entry {
                    background-color: antiquewhite;
                    padding: 5px;
                    margin: 10px 0;
                }
            </style>
            <div class="payment-history-box">
            </div>
        `;
    this._shadowRoot.innerHTML = template;
    this._paymentHistoryElement = this._shadowRoot.querySelector(".payment-history-box");
    this._shadowRoot.ownerDocument.addEventListener('payment-updated', (e) => this.loadHistory())
  }

  createPaymentEntry(data) {
    return `
            <div class="payment-entry">
                <ul>
                    <li>Type: ${data.type}</li>
                    <li>From: ${data.from}</li>
                    <li>To: ${data.to}</li>
                    <li>Amount: ${data.amount}</li>
                </ul>
            </div>
        `
  }

  refreshHistory(data){
    const html = Object.values(data).map(data => this.createPaymentEntry(data)).reverse().join('');
    this._paymentHistoryElement.innerHTML = html;
  }

  loadHistory(){
    return Ajax.get('https://localhost:8081/operations')
      .then((response) => {
        this.refreshHistory(JSON.parse(response));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  reset() {
    this._recipientElement.value = '';
    this._valueElement.value = '';
  }

  connectedCallback() {
    this.loadHistory();
  }
}

customElements.define('payment-history-box', PaymentHistoryBox);
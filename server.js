const fastify = require('fastify');
const fastifyAutoPush = require('fastify-auto-push');
const fs = require('fs');
const path = require('path');
const {promisify} = require('util');
const DB = require('diskdb');

const fsReadFile = promisify(fs.readFile);

const CERTS_DIR = path.join(__dirname, 'certs');
const PORT = 8080;

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection at:', reason.stack || reason)
});

process.on('uncaughtException', (reason) => {
  console.log('Unhandled exception at:', reason.stack || reason)
});

function configureApp(app) {
  app.addContentTypeParser('application/json', {parseAs: 'string'}, function (req, body, done) {
    try {
      var json = JSON.parse(body);
      done(null, json)
    } catch (err) {
      err.statusCode = 400;
      done(err, undefined)
    }
  });

  // app.register(fastifyAutoPush.staticServe, {
  //   root: path.join(__dirname, 'public')
  // });

  app.register((instance, opts, next) => {
    instance.register(fastifyAutoPush.staticServe, {
      root: path.join(__dirname, 'public'),
    });
    next()
  });

  app.register((instance, opts, next) => {
    instance.register(fastifyAutoPush.staticServe, {
      root: path.join(__dirname, 'node_modules'),
      prefix: '/scripts/', // optional: default '/'
    });
    next()
  })
}

async function createServerOptions() {
  const readCertFile = (filename) => fsReadFile(path.join(CERTS_DIR, filename));
  const [key, cert] = await Promise.all([readCertFile('localhost.key'), readCertFile('localhost.crt')]);
  return {key, cert};
}

async function startDB() {
}

async function main() {
  const {key, cert} = await createServerOptions();

  // Browsers support only https for HTTP/2.
  const staticApp = fastify({https: {key, cert}, http2: true});
  const app = fastify({https: {key, cert}, http2: false});

  configureApp(staticApp);

  const db = DB.connect('./', ['testdb']);

  app.post('/operation', async (req, res) => {
    const isTransfer = (req.body.type || '').toLowerCase() === 'transfer';
    let amount = isTransfer && req.body.amount > 0 ? req.body.amount * -1 : req.body.amount;
    amount = parseFloat((amount || 0) + '');
    amount = isNaN(amount + '') ? 0 : amount;
    if(!req.body.to){
      res.status(400).send("Invalid receiver");
      return;
    }
    if(!req.body.from){
      res.status(400).send("Invalid sender");
      return;
    }
    if(!amount){
      res.status(400).send("Invalid amount");
      return;
    }
    db.testdb.save({
      type: req.body.type,
      amount: amount,
      from: req.body.from,
      to: req.body.to
    });
    res.status(201).send({status: "OK"});
  });

  app.get('/balance', async (req, res) => {
    const operations = db.testdb.find();
    const total = operations.reduce((prev, val) => {
      return prev + parseInt(val.amount)
    }, 0);
    res.status(200).send({total: total});
  });

  app.get('/operations', async (req, res) => {
    const operations = db.testdb.find({});
    res.status(200).send(operations);
  });

  app.register(require('fastify-cors'), {
    origin: '*'
  });

  await app.listen(PORT + 1);
  await staticApp.listen(PORT);
  console.log(`Listening on port ${PORT + 1}`);
  console.log(`Static files on port ${PORT}`);
}

main()
  .catch((err) => {
    console.error(err);
  });
# WebComponentsDojo

## Useful Info

#### Custom components

A custom component is a class that extends `HTMLElement`.

Inside the `constructor`, we define all the functionality the element will have when an instance of it is instantiated.

##### Lifecycle callbacks

- `connectedCallback`: Invoked each time the custom element is appended into a document-connected element.  
- `attributeChangedCallback`: Invoked when one of the custom element's attributes is added, removed, or changed.
- `adoptedCallback`: Invoked each time the custom element is moved to a new document.
- `disconnectedCallback`: Invoked each time the custom element is disconnected from the document's DOM.

## Kata

Tyro is implementing a web banking platform and decided to go for webcomponents as main frontend technology.

#### Components
As first requirements, you must implement three main webcomponents:
1. A box, in the main page, where the user can transfer money to other users.
2. A box in the page header containing the current balance of the user's account.
3. A box in the right side of the website containing the account transfer history.

#### Functionality

- When the page is opened, the webcomponents must render the current state of the data in the server
- When a transfer is made by submitting a transfer in component 1, components 2 and 3 must automatically be updated to display the current state of the data in the server.


![alt text](./design.png)


##### Have fun!
